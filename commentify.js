function commentify(site, fid) {
  var stylepath = 'http://dev1.lowcountrynewspapers.com/sites/dev1.lowcountrynewspapers.com/modules/commentify/';
  var imagepath = 'http://dev1.lowcountrynewspapers.com/sites/dev1.lowcountrynewspapers.com/modules/commentify/images/';

  // Some customization is required here. Change the first element to your target for displaying the comments list
	$("#commentcontainer").append('<div id="comments"><span><img id="togglecomments" src="' + imagepath + 'minus.png" title="show/hide comments"/> Comment on this</span></div>');
	$("head").append('<link rel="stylesheet" href="' + stylepath + 'comments.css" type="text/css" media="screen" title="no title" charset="utf-8" />');
	$.getJSON(site + "/" + fid + "&callback=?",
		function(data){

			$.each(data.comments, function(i,comment){
				var box = $("<div></div>").appendTo("#comments");
				$('<p></p>').text(comment.comment.replace(/(<([^>]+)>)/ig,"")).appendTo(box);     
				$('<div class="subj"></div>').text((i+1) + ") " + comment.subject).prependTo(box.find("p"));
				var cdate = new Date();
				cdate.setTime(comment.timestamp*1000);
				$('<div></div>').html('Posted by <a href="' + data.url.replace(/story.*/,"user/") + comment.name + '">' + comment.name + '</a>, ' + cdate.toLocaleString() ).addClass("info").appendTo(box);
			}); 
			
			// Uncomment the following if you have jquery cookie plugin
			
      // if($.cookie('showinlinecomments') == 0){ 
      //  $("#comments > *:gt(0)").hide();
      //  $("#comments span img").attr("src",imagepath + "plus.png");
      // }

      // Again with the changes. #commentcounter element is where to display count
			$("<a>").attr('href','#comments').text('(0) Comments').appendTo("#commentcounter");
			
            if (data.count > 0) {
				$("#comments span").append(" (Showing " + data.comments.length + " of " + data.count +")");
                $("#commentcounter a").text('('+data.count+') Comments');
	        	$("#comments").append('<div class="tools"><a href="' + data.url.replace(/node/,"comment/reply") + '">Add your own comment</a> (Free Registration Required) | <a href="' + data.url + '">View all</a></div>');
			} else {
				$("#comments").append('<div class="tools"><a href="javascript:document.commentform.submit();">No comments yet, be the first</a></div>'); 
			}  	
		}
  	);
	$('#comments span').toggle(
		function(){
			$("#comments > *:gt(0)").slideUp(); 
			$("#comments span img").attr("src",imagepath + "plus.png");
      // $.cookie('showinlinecomments',0,{ expires: 365 });
		},
		function(){
			$("#comments > *:gt(0)").slideDown();
			$("#comments span img").attr("src",imagepath + "minus.png");
      // $.cookie('showinlinecomments',1,{ expires: 365 });
		}
	);

}