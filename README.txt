Commentify.module
1 Drupal Commentify Module
    1.1 By: Chris Yates, chris@christianyates.com
          <mailto:chris@christianyates.com>
    1.2 Licensed under the GNU/GPL License
    1.3 Sponsored by The Beaufort Gazette (beaufortgazette.com
          <http://beaufortgazette.com/>) and The Island Packet
          (islandpacket.com <http://islandpacket.com/>)
    1.4 This software is offered with no warranty expressed or
          implied. User agrees to indemnify, hold harmless, and defend
          the author and his employer or agents against any and all
          claims arising out of the exercise of any rights under this
          Agreement, including, without limiting the generality of the
          foregoing, against any damages, losses, or liabilities
          whatsoever with respect to death or injury to person or
          damage to property arising from or out of the possession,
          use, or operation of the software or Licensed Program(s) by
          the user or its customers
2 About
      Commentify provides a way to use Drupal as a commenting solution
      for third-party web sites, whether they're static pages, a
      different CMS, or another Drupal installation. It was developed
      to add comments to newspaper web sites served from a proprietary
      CMS.
3 Installation
    3.1 Download
          <http://christianyates.com/commentify.tar.gz>
        3.1.1 Contents
            3.1.1.1 commentify.info - drupal info file
            3.1.1.2 commentify.install - creates a single database table
                  to support the commentify module
            3.1.1.3 commentify.js - an example javascript that renders
                  the output of Commentify.module on third party pages,
                  when the 'list' display option is chosen. Requires
                  jquery version 1.2.x, which can be downloaded from
                  jquery.com <http://jquery.com/>. This is a newer
                  version of jquery than is provided with the Drupal
                  5.x installation by default. You are free to use this
                  script, though please change any references to static
                  files to point to your own server.
            3.1.1.4 commentify.module - Drupal module
            3.1.1.5 comments.css - CSS file that is called by
                  commentify.js
            3.1.1.6 images directory - contains + - issues referenced by
                  commentify.js to support slideup/down behavior
            3.1.1.7 README.txt - this file
    3.2 Install
        3.2.1 Unpack commentify.tar.gz into your Drupal modules
              directory
        3.2.2 Enable the module
4 How it works
      Commentify handles the creation of Drupal nodes and their
      association with a numeric id provided by a third-party site.
      This id must be a unique key. An example could be a nid from
      another Druapl site, or any other unique key provided by a CMS.

      When a POST request is made to Commentify.module, such as
      http://mysite.com/commentify/add
      <http://mysite.com/commentify/fid/12345,> with fid=1234
      Commentify will query the commentify table in the Drupal database
      either return a Drupal nid associated with that fid, or create a
      new Drupal node if no association exists. The user will
      subsequently have the opportunity to add a comment to this node.

      Commentify also supplies a means to display comments and/or
      comment count back to the third-party site through two means. In
      its simplest form, it provides a simple javascript file that uses
      document.write() to render a comment count in the location you
      choose. Alternately you can choose to supply a JSON feed of
      comments on the associated node via the same URL, which can be
      parsed with javascript or by the third-party site's scripting
      language of choice. A javascript file named commentify.js is
      provided with this distribution that can be used for this
      purpose. It is (almost) identical to the solution in use on The
      Beaufort Gazette's web site,
      <http://www.beaufortgazette.com/opinions/letters/story/197186.html
      >.

      Finally, Commentify.module provides a cron hook that will clean
      up nodes created by the module that have not received comments.
      This prevents your site from having a large number of stub nodes
      with no comments attached.
5 Configuration
    5.1 Node Creation Settings
        5.1.1 Content Type
              Commentify will create nodes when the commentify/add url
              is posted to with a foreign id (fid), a title and body.
              You can either use an existing content type, or create a
              new one especially for use with Commentify.module. This
              content type should NOT be used for any other purpose.
        5.1.2 Site URL
              The domain from which to accept commentify posts. This is
              a low-tech method of spam abatement, which we have found
              reasonably effective. If you supply a second-level domain
              name such as islandpacket.com, foo.islandpacket.com will
              also support commentify submissions.
        5.1.3 Default Options
              This setting reflects the post settings for the content
              type. This allows nodes created by Commentify.module to
              have different settings than the content type's default.
        5.1.4 Comment Options
              This does the same as 5.1.3 for comment settings.
              Generally, it would be left as read/write
    5.2 Cleanup Settings
          Commentify.module provides a cron hook that will
          automatically delete nodes created by the module that have
          not received comments. Alternately, you can call
          <http://mysite.com/commentify/cleanup>
          directly to run the cleanup functions outside of Drupal's
          cron hook process.
    5.3 Display Settings
        5.3.1 Display Type
              Commentify.module will supply a javascript which is
              accesible from
              <http://mysite.com/commentify/fid/1234,> where 1234 is an
              id supplied by the third-party CMS or web site
            5.3.1.1 Text
                  Commentify will return a javascript document.write()
                  directive that simply writes the number of comments
                  that the node has received, or zero if no Drupal node
                  has been created.
            5.3.1.2 List
                  Commentify will return a JSON file containing
                  - The URL of the associated drupal node, if one exists
                  - A list of comments, with creation time and author,
                  if they exist

                  You may adapt the included commentify.js script to
                  render this JSON file into HTML using jquery 1.2+, or
                  you can parse the file with the scripting language of
                  your choice.
        5.3.2 Number of comments to return
              Applies only to the 'list' display type. It is the total
              number of comments to return in the JSON file, though a
              total comment count is also provided, so you can print
              out informational text such as "Showing 1-20 of 60
              Comments".
        5.3.3 Display comment form
              Returns a boolean in the JSON file indicating whether or
              not to show a comment form on the third party site.
              Parsing and rendering this element is left as an exercise
              for the reader, and has not been implemented yet in
              production.
6 Third Party Integration
      Commentify.module provides a framework for story commenting, but
      it is up to the user to integrate its input and output into a
      third-party system, and the methodology will vary from one
      installation to another. A an example javascript implementation
      is provided as a starting point for such an integration.
    6.1 Node creation
          Commentify.module expects to receive three peices of
          information to be POSTed to the commentify/add menu path: fid
          (the unique foreign key), a headline and a body, like so:

          <form action="http://www2.beaufortgazette.com/commentify/add"
          method="post" name="commentform">
          	<input type="hidden" name="headline" value="Building Whale
          Branch will cost district a vote" />
          	<input type="hidden" name="body" value="The Beaufort County
          School District is considering ways to sell its proposed
          $162.7 million referendum to voters.  Read the story &lt;a
          href=&quoti;http://www.beaufortgazette.com/opinions/letters/story/197186.
          html&quot;&gt;here&lt;/a&gt;.>" />
          	<input type="hidden" name="fid" value="197186" />
          </form>

          The contents of the headline and body fields should be
          properly escaped. Commentify.js provides methods for
          submitting this form.
    6.2 Comment display
        6.2.1 Text
              If you choose the 'Text' display type, simply insert the
              following code into your third-party page to display the
              comment count in the selected location:

              <script type="text/javascript" src="http://mysite.com/commentify/fid/1234"></script>
        6.2.2 List
              Displaying data provided by the 'List' type is more
              complicated. A javascript file, commentify.js, is
              provided as an example of how to parse and render this
              data with jquery 1.2+. You may use this file as the basis
              of your own integration, but please change any references
              to static files to point to your own servers.
